import { GAME } from '../game/GameConsants';
import { currentTime } from '../game/helpers';
import { ONE_SECOND } from './constants';

// tslint:disable:no-console
let chalk;
if (GAME.isServer) {
	// tslint:disable-next-line:no-var-requires
	chalk = require('chalk');
}

export type LogLevel = 'trace' | 'debug' | 'info' | 'warn' | 'error' | 'fatal';

export const LVL_CLR = {
	trace: 'magenta',
	debug: 'blue',
	info: 'green',
	warn: 'yellow',
	error: 'red',
	fatal: 'red',
};

export const LVL_BG = {
	trace: 'black',
	debug: 'black',
	info: 'black',
	warn: 'black',
	error: 'black',
	fatal: 'white',
};

export const STR_CLR = {
	trace: 'gray',
	debug: 'gray',
	info: 'white',
	warn: 'white',
	error: 'white',
	fatal: 'white',
};

export const STR_BG = {
	trace: 'black',
	debug: 'black',
	info: 'black',
	warn: 'black',
	error: 'black',
	fatal: 'black',
};

export const rank = {
	fatal: 6,
	error: 5,
	warn: 4,
	info: 3,
	debug: 2,
	trace: 1,
};

export const PROCESS_LOG_LEVEL: LogLevel = 'info';

export const limit: any = {};
export class Log {

	private level = PROCESS_LOG_LEVEL;

	public constructor(
		public name: string,
	) {}

	public fatalServer(...str: any[]) {
		if (GAME.isServer) {
			this.write('error', this.convert(str));
		}
	}

	public fatal(...str: any[]) {
		if (GAME.isServer) {
			this.write('error', this.convert(str));
		} else {
			console.error(...str);
		}
	}

	public errorServer(...str: any[]) {
		if (GAME.isServer) {
			this.write('error', this.convert(str));
		}
	}
	public error(...str: any[]) {
		if (GAME.isServer) {
			this.write('error', this.convert(str));
		} else {
			console.error(...str);
		}
	}

	public warnServer(...str: any[]) {
		if (GAME.isServer) {
			this.write('warn', this.convert(str));
		}
	}
	public warn(...str: any[]) {
		if (GAME.isServer) {
			this.write('warn', this.convert(str));
		} else {
			console.warn(...str);
		}
	}

	public infoServer(...str: any[]) {
		if (GAME.isServer) {
			this.write('info', this.convert(str));
		}
	}
	public info(...str: any[]) {
		if (GAME.isServer) {
			this.write('info', this.convert(str));
		} else {
			console.info(...str);
		}
	}

	public debugServer(...str: any[]) {
		if (GAME.isServer) {
			this.write('debug', this.convert(str));
		}
	}
	public debug(...str: any[]) {
		if (GAME.isServer) {
			this.write('debug', this.convert(str));
		} else {
			console.debug(...str);
		}
	}

	public traceServer(...str: any[]) {
		if (GAME.isServer) {
			this.write('trace', this.convert(str));
		}
	}
	public trace(...str: any[]) {
		if (GAME.isServer) {
			this.write('trace', this.convert(str));
		} else {
			console.trace(...str);
		}
	}

	public logInfoLimit(id: string, seconds: number, ...str: any[]) {
		if (!limit[id] || currentTime() > limit[id] + seconds * ONE_SECOND) {
			limit[id] = currentTime();
			this.info(str);
		}
	}

	private convert(arr: any[]): string[] {
		return arr.map((obj) => {
			if (typeof obj === 'string') {
				return obj;
			}

			try {
				return JSON.stringify(obj);
			} catch (e) {
				return '{{ Circular JSON Structure }}';
			}
		});
	}

	private write(
		lvl: LogLevel,
		txt: string[],
	) {
		if (rank[lvl] < rank[this.level]) {
			return;
		}

		const lvlText = chalk.keyword(LVL_CLR[lvl]).bgKeyword(LVL_BG[lvl])(lvl);
		const strText = chalk.keyword(STR_CLR[lvl]).bgKeyword(STR_BG[lvl])(txt.join(', '));

		// tslint:disable-next-line:no-console
		console.log(`${ chalk.grey('  [') } ${ lvlText } ${ chalk.grey(']  ' + (this.name || 'LOGGER') + ':') } ` +
			`${ strText }`);
	}

}
