import { PubResult } from '../../common/pub/v1/PubResult';
import { PubParams } from '../interfaces/PubParams';
import { handleClientInput } from './pub/ClientInput';
import { handleClientState } from './pub/ClientState';
import { handleHearbeat } from './pub/HeartbeatRoute';
import { handleInitClient } from './pub/InitClientRoute';

export const pubRoutes: { [action: string]: (params: PubParams) => PubResult | Promise<PubResult> } = {
	initClient: handleInitClient,
	heartbeat: handleHearbeat,
	clientInput: handleClientInput,
	clientState: handleClientState,
};
