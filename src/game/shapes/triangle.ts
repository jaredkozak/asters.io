import { Vector } from 'sat';
import { Polygon } from './polygon';

export class Triangle extends Polygon {

	public type: 'triangle' = 'triangle';

	public constructor(
		public width: number,
		public height: number,
	) {
		super([]);

		this.points = [
			new Vector(0, -this.height / 2),
			new Vector(this.width / 2, this.height / 2),
			new Vector(-this.width / 2, this.height / 2),
		];
	}

}
