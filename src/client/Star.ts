import { Vector } from 'sat';

export class Star {

	public constructor(
		public pos: Vector,
		public size: number,
		public distance: number,
	) {}
}
