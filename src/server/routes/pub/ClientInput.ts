import { EVENTS } from '../../../common/events';
import { ClientInput } from '../../../common/pub/v1/ClientInput';
import { PubParams } from '../../interfaces/PubParams';

export async function handleClientInput(params: PubParams): Promise<ClientInput.Output> {
	const { client, server } = params;
	const data = params.data.data as ClientInput.Input;

	if (data.start) {
		server.game.sendEvent(EVENTS.INPUT_START, data.inputType, client.id);
	} else {
		server.game.sendEvent(EVENTS.INPUT_END, data.inputType, client.id);
	}
	return {};
}
