
export interface PubResult {
	error?: PubError;
	data?: any;
}

export interface PubError {
	msg: string;
	code: number;
}
