import { ObjectCharacteristics, ShipCharacteristics } from '../game/characteristics';

export const defaultObjectCharacteristics: ObjectCharacteristics = {
	ROTATION_DRAG: 0.5,
	DRAG: 0.4,
	VIEW_DISTANCE: 1000,
};

export const defaultShipCharacteristics: ShipCharacteristics = {
	...defaultObjectCharacteristics,
	FWD_THRUST: 600,
	REVERSE_THRUST: 100,
	SIDE_THRUST: 50,
	ROTATE_THRUST: 8,
	TORPEDO_SPEED: 500,

	WIDTH: 50,
	HEIGHT: 100,
};
