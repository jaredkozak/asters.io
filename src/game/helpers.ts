
import { GAME } from './GameConsants';

let nodePerformance;

if (GAME.isServer) {
	// tslint:disable-next-line:no-var-requires
	const { performance } = require('perf_hooks');
	nodePerformance = performance;
}

export function currentTime(): number {
	if (GAME.isBrowser) {
		return performance.now();
	} else {
		return nodePerformance.now();
	}
}
