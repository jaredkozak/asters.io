import { Circle as SatCircle, Vector } from 'sat';
import { GameObject } from '../GameObject';

export class Circle {

	public type: 'circle' = 'circle';
	public color = 'blue';

	public constructor(
		public radius: number,
	) {}

	public getSat(go: GameObject) {
		const circle = new SatCircle(go.pos.clone(), this.radius);
		return circle;
	}
}
