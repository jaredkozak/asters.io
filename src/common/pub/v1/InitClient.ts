import { PubResult } from './PubResult';

// tslint:disable-next-line:no-namespace
export namespace InitClient {
	export const action = 'initClient';

	export interface Input {
		clientId?: string;
		clientSecret?: string;
	}

	export interface ClientConnection {
		id: string;
		secret: string;
		name: string;
	}

	export interface Output extends PubResult {
		data: ClientConnection;
	}
}
