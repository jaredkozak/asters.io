
export interface Drawable {

	type: 'circle';
	radius: number;
	x: number;
	y: number;
	r: number;

	xOffset?: number;
	yOffset?: number;

}
