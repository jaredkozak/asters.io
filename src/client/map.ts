import { Vector } from 'sat';
import { getMousePos } from '.';

const HEIGHT = 2;
const WIDTH = 1.73205;

export const MAX_MAP_ZOOM = 6;
export const MIN_MAP_ZOOM = 3;
export const mapPosition = new Vector(0, 0);

const SIZE = 10;

const V = 0.5;
// tslint:disable-next-line:no-magic-numbers
const HEX_RATIO = 2 * V * Math.cos(Math.PI / 6);
const DOUBLE_HEX_RATIO = 2 * HEX_RATIO;

export interface Space {
	x: number;
	y: number;
	id: string;
	name: string;

	// position on screen
	pos?: Vector;
	// scale size of hexagon on screen
	size?: number;
	// whether the mouse is currently hovering over this space
	hovering?: boolean;
	// Whether the user is currently in this system
	currentSystem?: boolean;
}

export const map: { [ id: string ]: Space } = {
	a: {
		x: 0,
		y: 0,
		id: 'a',
		name: 'A',
	},
	b: {
		x: 1,
		y: 0,
		id: 'b',
		name: 'B',
	},
	c: {
		x: 2,
		y: 0,
		id: 'c',
		name: 'C',
	},

	d: {
		x: 0,
		y: 1,
		id: 'd',
		name: 'D',
	},
	e: {
		x: 1,
		y: 1,
		id: 'e',
		name: 'E',
	},
	f: {
		x: 2,
		y: 1,
		id: 'f',
		name: 'F',
	},
};
export const currentSpace = map.c;

// tslint:disable:no-magic-numbers
export async function drawMap($: CanvasRenderingContext2D, c, input: {
	mapZoom?: number;
}) {
	const mapZoom = input.mapZoom;
	const zoomSize = mapZoom * SIZE;

	for (const space of Object.values(map)) {
		const pos = getGridHexagonPosition(
			zoomSize,
			c.width / 2, c.height / 2,
			space.x - currentSpace.x, space.y - currentSpace.y,
		);

		space.pos = pos;
		space.size = zoomSize;
		space.currentSystem = currentSpace === space;

		space.hovering = mouseIntersects(space);
		drawSpace(space);
	}

	for (const space of Object.values(map)) {
		if (space.hovering) {
			// Draw information of system to screen

			const padding = 3;
			const ml = 3;
			const mt = 3;

			const height = 18;
			const textA = `SYSTEM - ${ space.name }`;
			const textB = `${ space.id } - ${ space.x }, ${ space.y }`;
			const textASize = $.measureText(textA);
			const textBSize = $.measureText(textB);
			const width = Math.max(textASize.width, textBSize.width);

			const mouse = getMousePos();
			$.beginPath();
			$.rect(mouse.x + ml, mouse.y + mt, width + padding * 2, height * 2 + padding * 3);
			$.strokeStyle = 'none';
			$.fillStyle = 'rgba(65, 74, 76,0.7)';

			$.stroke();
			$.fill();

			$.beginPath();
			$.fillStyle = 'black';
			$.textAlign = 'left';
			$.font = `${ height }px Helvetica`;
			$.fillText(textA, mouse.x + padding + ml, mouse.y + padding + mt);
			$.fillText(textB, mouse.x + padding + ml, mouse.y + height + padding * 2 + mt);
		}
	}

	// const mousePos = getMousePos();

	// drawHexagonGrid({
	// 	size: mapZoom * SIZE,
	// });

	/**
	 * MAP SUBFUNCS:
	 */

	function mouseIntersects(space: Space) {
		const mouse = getMousePos();
		if (!space.pos || !space.size) { return false; }
		const h = HEX_RATIO * space.size;
		const v = V * space.size;

		const diff = mouse.sub(space.pos);
		const abs = new Vector(Math.abs(diff.x), Math.abs(diff.y));
		// bounding box check
		if (abs.x > h || abs.y > 2 * v) { return false; }

		// check hexagon angles
		const p2 = new Vector(h, v);
		const n = new Vector(-v, -h);
		const m = abs.sub(p2);
		return n.dot(m) >= 0;
	}

	function getGridHexagonPosition(size: number, left: number, top: number, x: number, y: number) {

		const pos = new Vector(0, 0);
		if (y % 2 === 0) {
			pos.x = x * DOUBLE_HEX_RATIO;
			pos.y = y * 1.5;
		} else {
			pos.x = x * DOUBLE_HEX_RATIO + HEX_RATIO;
			pos.y = y * 1.5;
		}

		pos.scale(size);

		pos.add(new Vector(left, top));

		return pos;
	}
	function drawSpace(space: Space) {
		if (!space.size || !space.pos) { return; }
		let fill = '#222';
		const stroke = '#000';
		if (space.hovering) {
			fill = '#fff';
		} else if (space.currentSystem) {
			fill = '#444';
		}

		drawHexagon(space.size, space.pos, fill, stroke);
	}

	function drawHexagon(size: number, position: Vector, fill: string, stroke: string) {

		const points = [
			[0, 1],
			[HEX_RATIO, V],
			[HEX_RATIO, -V],
			[0, -1],
			[-HEX_RATIO, -V],
			[-HEX_RATIO, V],
		];

		$.beginPath();

		for (let i = 0; i < points.length; i++) {
			const pointPos = new Vector(points[i][0], points[i][1]);
			pointPos.scale(size);
			pointPos.add(position);

			if (i === 0) {
				$.moveTo(pointPos.x, pointPos.y);
			} else {
				$.lineTo(pointPos.x, pointPos.y);
			}
		}

		$.strokeStyle = stroke;
		$.fillStyle = fill;
		$.fill();
		$.stroke();
		$.closePath();
	}
}
