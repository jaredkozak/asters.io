export interface SubMessage {
	action: string;
	data?: any;
}
