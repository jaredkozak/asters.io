import { currentTime } from './helpers';

export interface Handler {

	// Rate in updates milliseconds between updates
	period: number;

	// For internal use only
	_lastUpdate?: number;
	_lost?: number;

	update(delta: number): void | Promise<void>;
}

export class Loop {

	public running = false;

	public handlers: Handler[] = [];

	public async start() {
		this.running = true;
		this.run();
	}

	public async run() {
		const tick = async () => {
			const now = currentTime();
			for (const h of this.handlers) {
				h._lost = !h._lost ? 0 : h._lost;
				h._lastUpdate = h._lastUpdate === undefined ? now : h._lastUpdate;
				const delta = now - h._lastUpdate;
				if (delta + h._lost >= h.period) {
					h._lastUpdate = now;
					h._lost = delta - h.period < 0 ? 0 : delta - h.period;
					await h.update(delta);
				}
			}

			if (this.running) {
				setTimeout(tick, 0);
			}
		};

		setTimeout(tick, 0);
	}

	public addHandler(h: Handler) {
		this.handlers.push(h);
	}
}
