import { Errors } from '../../common/errors';
import { PubMessage } from '../../common/pub/PubMessage';
import { ClientConnection } from '../ClientConnection';
import { PubParams } from '../interfaces/PubParams';
import { ServerParams } from '../interfaces/ServerParams';
import { pubError } from './pubError';
import { pubRoutes } from './pubRoutes';

export async function initPubRoutes(params: ServerParams) {

	params.app.post('/server/pub', async (req, res) => {

		let responseData = pubError(Errors.InvalidMessage);
		if (req.body && req.body.action) {
			const body = req.body as PubMessage;
			const handler = pubRoutes[body.action];
			if (handler) {
				let client: ClientConnection;
				if (body.clientId && body.clientSecret) {
					client = params.network.findAndValidate(body.clientId, body.clientSecret);
					if (client) {
						client.heartbeat();
					}
				}
				const pubParams: PubParams = {
					server: params,
					client,
					data: body,
				};
				responseData = await handler(pubParams);
				if (responseData === undefined) {
					responseData = pubError(Errors.NoResponse);
				}
			} else {
				responseData = pubError(Errors.NoRoute);
			}
		}

		res.send(responseData);
		res.end();

	});
}
