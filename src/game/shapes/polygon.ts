import { Polygon as SatPoly, Vector } from 'sat';
import { GameMath } from '../GameMath';
import { GameObject } from '../GameObject';
import { Shape } from '../Shape';

export class Polygon implements Shape {

	public type: 'polygon' | 'triangle' = 'polygon';
	public color = 'blue';

	public constructor(
		public points: Vector[],
	) {}

	/**
	 * Retrieve the points
	 * @param oX Offset X
	 * @param oY Offset Y
	 * @param rad Rotation in radians
	 */
	public getPoints(oX: number, oY: number, rad: number) {
		const offset = new Vector(oX, oY);
		return this.points.map((pReal) => {
			const p = new Vector(pReal.x, pReal.y);
			p.add(offset);
			return GameMath.rotatePoint(rad, p.x, p.y, oX, oY);
		});
	}

	public clonePoints() {
		return this.points.map((p) => new Vector(p.x, p.y));
	}

	public getSat(go: GameObject) {
		const poly = new SatPoly(go.pos, this.clonePoints());
		poly.setAngle(go.r);

		return poly;
	}

}
