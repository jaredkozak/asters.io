import { CLIENT_REMOVAL_INTERVAL, NETWORK_REPOP_PERIOD, NETWORK_UPDATE_PERIOD } from '../common/constants';
import { Log } from '../common/log';
import { Game } from '../game/Game';
import { currentTime } from '../game/helpers';
import { ClientConnection } from './ClientConnection';

// dont change these
const ID_LENGTH = 36;
const SECRET_LENGTH = 144;

export class ServerNetwork {

	public clients: ClientConnection[] = [];
	public activeClients: ClientConnection[] = [];
	private log = new Log('Network');

	public constructor(
		private game: Game,
	) {
		game.loop.addHandler({
			period: NETWORK_UPDATE_PERIOD,
			update: (delta) => { this.update(delta); },
		});

		game.loop.addHandler({
			period: NETWORK_REPOP_PERIOD,
			update: (delta) => { this.repop(delta); },
		});
	}

	public addClient(con: ClientConnection) {
		this.clients.push(con);
		this.activeClients.push();
	}

	public find(id: string) {
		return this.clients.find((c) => c.id === id);
	}

	public findAndValidate(id, secret) {
		id = Buffer.from(id);
		secret = Buffer.from(secret);
		this.log.trace(id.length, secret.length, ID_LENGTH, SECRET_LENGTH);

		// Id and secret must be of the correct length (they are all the same)
		if (id.length !== ID_LENGTH || secret.length !== SECRET_LENGTH) {
			return undefined;
		}

		let found: ClientConnection;
		for (const client of this.clients) {
			if (client.validate(id, secret)) {
				found = client;
			}
		}

		return found;
	}

	/**
	 * Updates any changes to clients
	 */
	public update(delta) {
		for (const client of this.clients) {
			client.update();
		}
	}

	/**
	 * Completely repopulates clients
	 */
	public repop(delta) {

		const remove = [];
		for (const client of this.clients) {
			if (client.lastHeartbeat && client.lastHeartbeat + CLIENT_REMOVAL_INTERVAL < currentTime()) {
				remove.push(client);
			} else if (client.connected) {
				client.repop();
			}
		}

		for (const client of remove) {
			const i = this.clients.indexOf(client);
			this.clients.splice(i, 1);
		}
	}

}
