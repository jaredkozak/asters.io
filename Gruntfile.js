module.exports = function(grunt) {

		// Project configuration.
	grunt.initConfig({
		pkg: grunt.file.readJSON('package.json'),
        ts: {
            default : {
				tsconfig: './tsconfig.json',
				options: {
					fast: 'always',
				},
            }
		},

		browserify: {
			vendor: {
				src: [],
				dest: 'public/js/vendor.js',
				options: {
					require: [ 'jquery', 'http-browserify', 'rxjs' ],
					// require: ['jquery'],
					// alias: {
					// 	momentWrapper: './lib/moments.js'
					// }
				}
			},
			common: {
				src: [
					'./dist/common/**/*.js',
				],
				dest: 'public/js/common.js',
				options: {
					external: ['rxjs', 'sat'],
					watch: true,
				}
			},
			game: {
				src: [
					'./dist/game/**/*.js',
				],
				dest: 'public/js/game.js',
				options: {
					external: ['rxjs'],
					watch: true,
				}
			},
			client: {
				src: [
					'./dist/client/**/*.js',
					'./dist/common/**/*.js',
				],
				dest: 'public/js/client.js',
				options: {
					external: ['jquery', 'http-browserify', 'rxjs'],
					watch: true,
				}
			},
		}, // \browserify
		  
		concat: {
			'public/js/main.js': [ 'public/js/vendor.js', 'public/js/common.js', 'public/js/game.js', 'public/js/client.js',  ],
			'public/js/logic.js': [ 'public/js/vendor.js', 'public/js/common.js', 'public/js/game.js',  ],
		},

		watch: {
			build: {
				files: [ 'src/**/*.ts' ],
				tasks: [ 'ts' ],
			},
			client: {
				//note that we target the OUTPUT file from watchClient, and don't trigger browserify
				//the module watching and rebundling is handled by watchify itself
				files: [ 'public/js/client.js', 'public/js/common.js', 'public/js/game.js' ],
				tasks: ['concat']
			},
		},

		nodemon: {
			client: {
			  script: 'dist/index.js',
			  options: {
				nodeArgs: ['--debug'],
				ignore: [
					'node_modules/**',
					'dist/client/**',
					'dist/game/**' // comment in server dev mode
				],
				ext: 'js',
				watch: ['dist'],
			  }
			},
		},

		concurrent: {
			options: {
			  logConcurrentOutput: true
			},
			client: {
			  tasks: ["watch:build", "watch:client"]
			},
			server: {
			  tasks: ["watch:build", "nodemon:dev"]
			},
			both: {
				tasks: ["watch:build", "watch:client", "nodemon:client"]
			}
		  }
	});

	// Load the plugin that provides the "uglify" task.
	// grunt.loadNpmTasks('grunt-contrib-uglify');

	// Default task(s).
	// grunt.registerTask('default', ['uglify']);

	grunt.loadNpmTasks("grunt-ts");
	grunt.loadNpmTasks('grunt-browserify');
	grunt.loadNpmTasks('grunt-contrib-concat');
	grunt.loadNpmTasks('grunt-contrib-watch');
	grunt.loadNpmTasks('grunt-concurrent');
	grunt.loadNpmTasks('grunt-nodemon');

    grunt.registerTask("default", [
		"ts",
		"browserify:vendor",
		"browserify:common",
		"browserify:game",
		"browserify:client",
		"concurrent:both",
	]);

	

    grunt.registerTask("client", [
		"ts",
		"browserify:vendor",
		"browserify:common",
		"browserify:game",
		"browserify:client",
		"concurrent:client",
	]);

    grunt.registerTask("build", [
		"ts",
		"browserify:vendor",
		"browserify:common",
		"browserify:game",
		"browserify:client",
		"concat",
	]);

};