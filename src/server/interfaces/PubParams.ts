import { PubMessage } from '../../common/pub/PubMessage';
import { ClientConnection } from '../ClientConnection';
import { ServerParams } from './ServerParams';

export interface PubParams {
	client: ClientConnection;
	server: ServerParams;
	data: PubMessage;
}
