import { Vector } from 'sat';
import { v1 as uuid } from 'uuid';
import { gameOptions } from '../../common/gameOptions';
import { GameObject } from '../GameObject';
import { Triangle } from '../shapes/triangle';

export class Torpedo extends GameObject {

	public shotBy: string;

	public constructor() {
		super('torpedo', undefined, 0, 0);
		this.shape = new Triangle(gameOptions.TORPEDO_WIDTH, gameOptions.TORPEDO_HEIGHT);
		this.shape.color = gameOptions.TORPEDO_COLOR;
		// tslint:disable-next-line:no-magic-numbers
		this.chars.DRAG = 0.01;
	}

	public setSpeed(angle, speed) {
		this.speed = new Vector(0, -1).rotate(angle).scale(speed);
		this.r = angle;
	}

	public onCollision(b: GameObject) {
		this.game.removeById(this.id);

		// TODO: Torpedo explosion
	}

}
