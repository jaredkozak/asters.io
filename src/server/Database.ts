import { connect } from 'mongoose';
import { config } from './config';

export async function initDB() {
	await connect(config.MONGO_URI);
}
