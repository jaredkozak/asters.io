import { DEFAULT_RADIUS } from './constants';

export class GameEvent {

	public x: number;
	public y: number;
	public radius = DEFAULT_RADIUS;

	public constructor(
		public type: string,
		public data: any,
	) {

	}

	public toJSON() {
		return JSON.stringify({
			...this.data,
			type: this.type,
		});
	}
}
