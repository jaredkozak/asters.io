export interface PubMessage {
	action: string;
	data: any;
	clientId?: string;
	clientSecret?: string;
}
