
export class GameMath {

	public static rotatePoint(rad: number, x: number, y: number, cx: number, cy: number) {

		const cos = Math.cos(-rad);
		const sin = Math.sin(-rad);
		const nx = (cos * (x - cx)) + (sin * (y - cy)) + cx;
		const ny = (cos * (y - cy)) - (sin * (x - cx)) + cy;
		return { x: nx, y: ny };
	}
}
