import { Log } from '../common/log';
import { GameObject } from './GameObject';
import { isCircle, isPolygon } from './Shape';
import { Circle } from './shapes/circle';
import { Polygon } from './shapes/polygon';
import { OBJECT_TYPES } from './types';

export function dataToGameObject(data: any): GameObject {

	const obj = new OBJECT_TYPES[data.type]();

	Object.assign(obj, data);
	obj.log = new Log(obj.log.name);
	if (isCircle(obj.shape)) {
		obj.shape = new Circle(obj.radius);
	} else if (isPolygon(obj.shape)) {
		obj.shape = new Polygon(obj.points);
	}
	Object.assign(obj.shape, data.shape);
	return obj;
}
