
export const ONE_SECOND = 1000;
export const ONE_MINUTE = 60000;
export const TEN_SECONDS = 10000;

export const DEFAULT_RADIUS = 100;

// A little quicker than 60 UPS, but is an even number;
export const GAME_UPDATE_PERIOD = 15;

export const ORIGIN = 'http://localhost:5000';

export const MAX_SPEED = 1000;
export const MAX_ROTATION_SPEED = 10;

export const NETWORK_UPDATE_PERIOD = 30;
export const NETWORK_REPOP_PERIOD = 50;

export const CLIENT_TOLERANCE = 0.05;
export const CLIENT_REMOVAL_INTERVAL = 100000;
