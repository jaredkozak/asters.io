import { initDB } from './Database';
import { initGameServer, startGameServer } from './GameServer';

async function start() {
	await initDB();
	await initGameServer();
	await startGameServer();
}

start();
