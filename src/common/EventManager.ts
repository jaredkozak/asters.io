import { Subject } from 'rxjs';
import { filter } from 'rxjs/operators';
import { GameEvent } from './GameEvent';

export class EventManager {

	public events = new Subject<GameEvent>();

	public constructor() {}

	public listen(
		selector?: {
			area?: {
				x: number;
				y: number;
				radius: number;
			};
			types?: string[];
		},
	) {

		return this.events.asObservable()
			.pipe(
				filter((event) => {

					// Check if event is in event array.
					if (selector && selector.types) {
						const included = selector.types.includes(event.type);
						if (!included) {
							return false;
						}
					}

					// Check if event was in radius
					if (selector && selector.area) {
						// TODO: check if selector x,y in event bubble OR event x,y in selector bubble.
						const xDistance = selector.area.x - event.x;
						const yDistance = selector.area.y - event.y;
						const distance = Math.sqrt(xDistance * xDistance + yDistance * yDistance);

						const heard = distance <= selector.area.radius || distance <= event.radius;
						if (!heard) {
							return false;
						}

					}

					return true;
				}),
			);

	}

	public emit(
		type: string,
		data: any,
		args?: {
			area?: {
				x: number;
				y: number;
				radius: number;
			};
		},
	) {
		const event = new GameEvent(type, data);

		if (args && args.area) {
			event.x = args.area.x;
			event.y = args.area.y;
			event.radius = args.area.radius;
		}

		this.events.next(event);
	}
}
