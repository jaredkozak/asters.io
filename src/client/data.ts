import * as http from 'http';
import { Subject } from 'rxjs';
import { filter } from 'rxjs/operators';
import { ClientCredentials } from '../common/ClientCredentials';
import { PubMessage } from '../common/pub/PubMessage';
import { SubMessage } from '../common/sub/SubMessage';
import { Heartbeat } from '../common/sub/v1/Heartbeat';
import { CLIENT } from './clientConstants';

export const MINIMUM_HEARTBEAT_INTERVAL = 15000;

export class Data {
	public clientId: string;
	public clientSecret: string;

	private messagesReceived = new Subject<SubMessage>();
	private lastHeartbeat: number;
	private heartbeatCheck;

	public onReceive(action?: string) {
		return this.messagesReceived.asObservable().pipe(
			// Only pipe types user has subscribed to
			filter((value) => {
				return action ? value.action === action : true;
			}),
		);
	}

	public async publish<In, Out>(action: string, data?: In): Promise<Out> {
		const msg = JSON.stringify({
			action,
			data,
			clientId: this.clientId,
			clientSecret: this.clientSecret,
		} as PubMessage);

		return new Promise<Out>((resolve, reject) => {

			const opts: http.RequestOptions = {
				...CLIENT.server,
				path: '/server/pub',
				method: 'POST',
			};

			// @ts-ignore
			const req = http.request(opts, (res, b, c, d) => {
				let responseBody = '';

				res.on('readable', () => {
					responseBody += res.read();
				});

				res.on('end', () => {
					const responseObject = JSON.parse(responseBody);
					resolve(responseObject);
				});
			});

			req.setHeader('Content-Type', 'application/json');
			req.write(msg);
			req.end();
		});

	}

	public startSubscription() {

		if (!this.clientId && !this.clientSecret) {
			throw new Error('Must have client id and secret');
		}

		const utf = new TextDecoder('utf-8');
		const req = http.request({
			...CLIENT.server,
			path: '/server/sub',
			method: 'POST',
		}, (res) => {
			let dataBuf = '';
			res.on('data', (data) => {
				const str = dataBuf + utf.decode(data);
				const split = str.split('\n');
				dataBuf = split.splice(-1)[0];

				for (const strMsg of split) {

					try {
						const msg = JSON.parse(strMsg) as SubMessage;
						if (data.action === Heartbeat.action) {
							this.heartbeat();
						} else {
							this.messagesReceived.next(msg);
						}
					} catch (e) {
						console.error('Message not in JSON format.', strMsg);
					}
				}
			});

			res.on('error', (e) => {
				console.error(e);
				console.log('Restarting in 1.5 seconds.');
				const RESTART_INTERVAL = 1500;
				setTimeout(() => {
					this.startSubscription();
				}, RESTART_INTERVAL);
			});

			res.on('close', () => {
				console.log('closed');
			});

		});

		req.on('error', (e) => {
			console.error('Caught Error:', e);
		});

		req.setHeader('Content-Type', 'application/json');

		const creds = {
			id: this.clientId,
			secret: this.clientSecret,
		} as ClientCredentials;
		req.write(JSON.stringify(creds));

		req.end();
	}

	public heartbeat() {
		this.lastHeartbeat = performance.now();
		console.log('Heartbeat Received');

		clearTimeout(this.heartbeatCheck);

		this.heartbeatCheck = setTimeout(() => {
			console.error('Server timed out.');
		}, MINIMUM_HEARTBEAT_INTERVAL);
	}

}
