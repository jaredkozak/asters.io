import * as express from 'express';
import { Game } from '../../game/Game';
import { ServerNetwork } from '../network';

export interface ServerParams {
	network: ServerNetwork;
	game: Game;
	app: express.Express;
}
