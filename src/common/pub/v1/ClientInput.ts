
// tslint:disable-next-line:no-namespace
export namespace ClientInput {
	export const action = 'clientInput';

	export interface Input {
		inputType: number;

		// True for starting, false for stopping input
		start: boolean;
	}

	export interface Output {}
}
