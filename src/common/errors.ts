
export enum Errors {

	InvalidMessage,
	NoRoute,
	NoResponse,
}
