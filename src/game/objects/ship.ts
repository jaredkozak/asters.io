import { Vector } from 'sat';
import { MAX_ROTATION_SPEED, MAX_SPEED, ONE_SECOND } from '../../common/constants';
import { gameOptions } from '../../common/gameOptions';
import { INPUT } from '../../common/input';
import { defaultShipCharacteristics } from '../../common/ship_defaults';
import { ShipCharacteristics } from '../characteristics';
import { GameObject } from '../GameObject';
import { currentTime } from '../helpers';
import { Triangle } from '../shapes/triangle';
import { Torpedo } from './torpedo';

export class Ship extends GameObject {

	public name: string;
	public chars: ShipCharacteristics;

	public readonly width: number;
	public readonly height: number;
	public health = 100;

	public constructor(id: string) {
		super('ship', id, 0, 0);

		this.chars = {
			...this.chars,
			...defaultShipCharacteristics,
		};
		this.attr.score = 0;

		super.shape = new Triangle(this.chars.WIDTH, this.chars.HEIGHT);
		this.checkHealth();
	}

	public inputStart(input: INPUT) {
		this.attr[input] = true;
	}

	public inputEnd(input: INPUT) {
		this.attr[input] = false;
	}

	public async update(delta: number) {
		this.checkInputs(delta);
		this.checkMaxSpeed();

		super.update(delta);
	}

	public onCollision(b: GameObject) {
		if (b instanceof Torpedo) {
			const relSpeed = this.speed.sub(b.speed).len();
			let damage = relSpeed * gameOptions.TORPEDO_SPEED_DAMAGE_MULTIPLIER + gameOptions.TORPEDO_DAMAGE;
			const player = this.game.findById(b.shotBy) as Ship;
			if (this.health - damage < 0) {
				damage = this.health;
			}

			if (player !== this) {

				player.attr.score += damage;
			} else {
				player.attr.score -= damage;
			}

			if (damage === this.health) {
				const KILL_POINTS = 50;
				player.attr.score += KILL_POINTS;
			}

			this.doDamage(damage);

		}
	}

	public doDamage(damage: number) {
		this.health -= damage;
		this.checkHealth();
	}

	public heal(damage: number) {
		this.health += damage;
		this.checkHealth();
	}

	private checkHealth() {
		const HIGH_HEALTH = 80;
		const MEDIUM_HEALTH = 50;
		const LOW_HEALTH = 30;
		const CRITICAL_HEALTH = 10;

		if (this.health <= 0) {
			this.shape.color = 'white';
		} else if (this.health < CRITICAL_HEALTH) {
			this.shape.color = 'red';
		} else if (this.health < LOW_HEALTH) {
			this.shape.color = 'orange';
		} else if (this.health < MEDIUM_HEALTH) {
			this.shape.color = 'yellow';
		} else if (this.health < HIGH_HEALTH) {
			this.shape.color = 'blue';
		} else {
			this.shape.color = 'green';
		}

		if (this.health <= 0) {
			this.attr.destroyed = true;
			const SHOW_DEATH_FOR = 1500;
			this.setDeath(currentTime() + SHOW_DEATH_FOR);
		}

	}

	private checkInputs(delta: number) {

		const r = this.r;
		const doDampen = {
			rotate: true,
			fireUp: true,
			fireDown: true,
			fireSides: true,
		};

		if (this.attr[INPUT.FIRE_UP]) {
			const f = new Vector(0, -this.chars.FWD_THRUST || 0).rotate(r);
			this.applyForceAtCenter(f, delta);
			doDampen.fireDown = false;
		}

		if (this.attr[INPUT.FIRE_DOWN]) {
			const f = new Vector(0, this.chars.REVERSE_THRUST || 0).rotate(r);
			this.applyForceAtCenter(f, delta);
			doDampen.fireUp = false;
		}

		if (this.attr[INPUT.FIRE_LEFT]) {
			const f = new Vector(-this.chars.SIDE_THRUST || 0, 0).rotate(r);
			this.applyForceAtCenter(f, delta);
			doDampen.fireSides = false;
		}

		if (this.attr[INPUT.FIRE_RIGHT]) {
			const f = new Vector(this.chars.SIDE_THRUST || 0, 0).rotate(r);
			this.applyForceAtCenter(f, delta);
			doDampen.fireSides = false;
		}

		if (this.attr[INPUT.FIRE_ROTATE_LEFT]) {
			this.applyRotationalForce(-this.chars.ROTATE_THRUST, delta);
			doDampen.rotate = false;
		}
		if (this.attr[INPUT.FIRE_ROTATE_RIGHT]) {
			this.applyRotationalForce(this.chars.ROTATE_THRUST, delta);
			doDampen.rotate = false;
		}

		if (this.attr[INPUT.INERTIAL_DAMPENERS_ON]) {
			this.dampen(doDampen, delta);
		}

		if (this.attr[INPUT.SHOOT_TORPEDO]) {
			this.spawnTorpedo();
			this.attr[INPUT.SHOOT_TORPEDO] = false;
		}
	}

	private spawnTorpedo() {
		const direction = new Vector(0, 1).rotate(this.r);
		const TORPEDO_OFFSET = 10;
		const pos = this.pos
			.add(
				direction
				.clone()
				.scale(this.chars.HEIGHT / 2 + TORPEDO_OFFSET + gameOptions.TORPEDO_HEIGHT / 2)
				.reverse(),
			);

		const speed = this.speed.clone()
			.projectN(direction)
			.add(
				direction.clone()
				.reverse()
				.scale(this.chars.TORPEDO_SPEED),
			);

		const torpedo = new Torpedo();
		torpedo.shotBy = this.id;
		torpedo.generateId();
		torpedo.setGame(this.game);
		torpedo.setSpeed(this.r, speed.len());
		torpedo.setDeath(currentTime() + gameOptions.TORPEDO_LIFETIME * ONE_SECOND);
		torpedo.pos = pos;
		this.game.gameObjects.push(torpedo);
	}

	private dampen(
		doDampen: {
			rotate: boolean,
			fireDown: boolean,
			fireUp: boolean,
			fireSides: boolean,
		},
		delta: number,
	) {
		const m = delta / ONE_SECOND;
		if (doDampen.rotate) {
			const r = this.chars.ROTATE_THRUST;
			if (this.rSpeed < -r * m) {
				this.applyRotationalForce(r, delta);
			} else if (this.rSpeed < 0) {
				this.rSpeed = 0;
			} else if (this.rSpeed > r * m) {
				this.applyRotationalForce(-r, delta);
			} else if (this.rSpeed > 0) {
				this.rSpeed = 0;
			}
		}

		const direction = new Vector(0, 1).rotate(this.r).normalize();

		const dot = this.speed.normalize().dot(direction);

		if (doDampen.fireDown && dot < 0) {
			const forwardVel = this.speed.projectN(direction);
			const t = this.chars.REVERSE_THRUST;

			const force = forwardVel.normalize().reverse().scale(t);
			this.applyForceAtCenter(force, delta);
		}

		if (doDampen.fireUp && dot > 0) {
			const reverseVel = this.speed.projectN(direction.clone().reverse());
			let t = this.chars.FWD_THRUST;

			const impulse = reverseVel.clone().scale(this.mass).len();
			if (impulse < t) {
				t = impulse;
			}

			if (t > gameOptions.INERTIAL_DAMPENERS_STOP) {
				const force = reverseVel.clone().normalize().reverse().scale(t);
				this.applyForceAtCenter(force, delta);
			}
		}

		if (doDampen.fireSides) {
			const leftDirection = direction.clone().rotate(Math.PI / 2);

			const leftVel = this.speed.projectN(leftDirection);

			let t = this.chars.SIDE_THRUST;
			const impulse = leftVel.clone().scale(this.mass).len();
			if (impulse < t) {
				t = impulse;
			}

			if (t > gameOptions.INERTIAL_DAMPENERS_STOP) {
				const force = leftVel.clone().normalize().reverse().scale(t);
				this.applyForceAtCenter(force, delta);
			}
		}
	}

	private checkMaxSpeed() {

		const speed = this.speed;
		const speedLength = this.speed.len();
		if (speed.len() > MAX_SPEED) {
			const scale = MAX_SPEED / speedLength;
			speed.scale(scale);
			this.xSpeed = speed.x;
			this.ySpeed = speed.y;
		}

		if ( Math.abs(this.rSpeed) > MAX_ROTATION_SPEED ) {
			this.rSpeed *= MAX_ROTATION_SPEED / Math.abs(this.rSpeed);
		}
	}
}
