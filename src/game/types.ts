import { GameObject } from './GameObject';
import { Ship } from './objects/ship';
import { Torpedo } from './objects/torpedo';

export const OBJECT_TYPES: { [ name: string ]: any } = {
	ship: Ship,
	torpedo: Torpedo,
};

export function isShip(go: GameObject): go is Ship {
	return go.type === 'ship';
}
