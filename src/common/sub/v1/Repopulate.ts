import { GameObject } from '../../../game/GameObject';

export namespace Repopulate {

	export const action = 'repop';

	export interface HighScore {
		score: number;
		pos: { x: number, y: number };
		id: string;
		name: string;
	}

	export interface Event {
		nearby: GameObject[];
		currentPlayer: string;
		highScore?: HighScore;
	}

}
