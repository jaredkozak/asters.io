
import { randomBytes, timingSafeEqual } from 'crypto';
import { Subject } from 'rxjs';
import { v1 as uuid } from 'uuid';
import { Log } from '../common/log';
import { SubMessage } from '../common/sub/SubMessage';
import { Repopulate } from '../common/sub/v1/Repopulate';
import { Game } from '../game/Game';
import { Ship } from '../game/objects/ship';
import { ServerNetwork } from './network';

const SECRET_BYTE_LENGTH = 72;

export class ClientConnection {

	public readonly id: string;
	public name: string;
	public lastHeartbeat: number;
	public connected: boolean = false;
	public get ship() {
		return this.game.findById(this.id);
	}

	public outMessages = new Subject<SubMessage>();
	private outMessageQueue: SubMessage[] = [];

	private log = new Log('Client Connection');

	protected secret: string;

	public constructor(
		private game: Game,
		private network: ServerNetwork,
	) {
		this.id = uuid();
		network.addClient(this);
	}

	public genSecret() {
		const length = SECRET_BYTE_LENGTH;

		this.secret = randomBytes(length).toString('hex');
		return this.secret;
	}

	public setName(name: string) {
		this.name = name;
	}

	public heartbeat() {
		this.log.trace(`${ this.name } heartbeat`);
		this.lastHeartbeat = Date.now();
	}

	public validate(id: Buffer, secret: Buffer) {

		let valid = true;
		valid = timingSafeEqual(Buffer.from(this.id), id) && valid;
		valid = timingSafeEqual(Buffer.from(this.secret), secret) && valid;
		return valid;
	}

	public send<T>(action: string, data: T) {
		const msg = { action, data };

		if (this.connected) {
			this.outMessages.next(msg);
		} else {
			this.outMessageQueue.push(msg);
		}
	}

	public dispatchUnsentMessages() {
		let length = 0;
		if (this.connected) {
			for (const msg of this.outMessageQueue) {
				this.outMessages.next(msg);
				length++;
			}
		}
		this.outMessageQueue = [];
		return length;
	}

	public update() {
	}

	public repop() {
		if (!this.ship) {
			this.connected = false;
			return;
		}

		const nearby = this.getNearbyObjects();
		const highScore = this.findHighScore();
		this.send<Repopulate.Event>(Repopulate.action,  {
			nearby,
			currentPlayer: this.ship.id,
			highScore,
		});
	}

	public findHighScore() {
		let highScore: Repopulate.HighScore;
		for (const go of this.game.gameObjects) {
			if (go instanceof Ship
				&& go.attr.score !== undefined
				&& (highScore === undefined || highScore.score < go.attr.score)
				&& go !== this.ship
			) {

				highScore = {
					pos: go.pos,
					score: go.attr.score,
					name: go.name,
					id: go.id,
				};

			}
		}

		return highScore;
	}

	public getNearbyObjects() {

		if (!this.ship) {
			return;
		}

		const objs = [];
		const pos = this.ship.pos;
		for (const go of this.game.gameObjects) {
			objs.push(go.serialize());
			// const distance = pos.clone().sub(go.pos).len();
			// if (distance < this.ship.chars.VIEW_DISTANCE) {
			// 	objs.push(go.serialize());
			// }
		}

		return objs;

	}

}
