import { Errors } from '../../common/errors';
import { PubResult } from '../../common/pub/v1/PubResult';

export function pubError(error: Errors): PubResult {
	return {
		error: { msg: Errors[error], code: error },
	};
}
