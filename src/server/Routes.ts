import { Route } from './interfaces/Route';
import { PubRoute } from './routes/pub';
import { SubRoute } from './routes/sub';
import { ServerDependencies } from './ServerDependencies';

export class Routes {

	private routes: Route[] = [
		new SubRoute(),
		new PubRoute(),
	];

	public async loadRoutes(
		dep: ServerDependencies,
	) {

		for (const route of this.routes) {
			await route.register(dep);
			await route.load();
		}
	}

}
