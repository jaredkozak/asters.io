import * as sat from 'sat';
import { Vector } from 'sat';
import { v1 as uuid } from 'uuid';
import { ONE_SECOND } from '../common/constants';
import { Log } from '../common/log';
import { defaultObjectCharacteristics } from '../common/ship_defaults';
import { ObjectCharacteristics } from './characteristics';
import { Game } from './Game';
import { currentTime } from './helpers';
import { isCircle, isPolygon, Shape } from './Shape';

export class GameObject {
	public shape: Shape;
	protected game: Game;

	public r: number = 0;

	public xSpeed = 0;
	public ySpeed = 0;

	// Rotational speed in radians
	public rSpeed = 0;

	public xAccel = 0;
	public yAccel = 0;
	public rAccel = 0;
	public mass = 1;
	public bounce = 1;

	public spriteURL;
	public deathTime?: number;

	public attr: any = {};
	public chars: ObjectCharacteristics = {
		...defaultObjectCharacteristics,
	};

	public get pos() { return new Vector(this.x, this.y); }
	public set pos(pos: Vector) {
		this.x = pos.x;
		this.y = pos.y;
	}
	public get speed() { return new Vector(this.xSpeed, this.ySpeed); }
	public set speed(vel: Vector) { this.xSpeed = vel.x; this.ySpeed = vel.y; }
	protected log: Log;

	public constructor(
		public type: string,
		public id: string,
		public x: number,
		public y: number,
	) {
		this.log = new Log(type);
	}

	public setDeath(n) {
		this.deathTime = n;
	}

	public generateId() {
		this.id = uuid();
	}

	public setGame(game: Game) {
		this.game = game;
	}

	public async update(delta: number) {

		if (this.deathTime && currentTime() > this.deathTime) {
			this.game.removeById(this.id);
		}

		const m = delta / ONE_SECOND;
		this.applyDrag(m);

		this.xSpeed += this.xAccel * m;
		this.ySpeed += this.yAccel * m;
		this.rSpeed += this.rAccel * m;

		this.x += this.xSpeed * m;
		this.y += this.ySpeed * m;
		this.r += this.rSpeed * m;
	}

	public onCollision(b: GameObject, response: sat.Response) {}

	public applyForceAtCenter(force: Vector, delta: number) {

		const m = delta / ONE_SECOND;

		this.xSpeed += force.x * m * 1 / this.mass;
		this.ySpeed += force.y * m * 1 / this.mass;
	}

	public applyRotationalForce(r: number, delta: number) {
		const m = delta / ONE_SECOND;
		this.rSpeed += r * m;
	}

	public checkCollision(other: GameObject, response: sat.Response): boolean {
		let collides = false;
		if (isCircle(this.shape)) {
			if (isPolygon(other.shape)) {
				collides = sat.testCirclePolygon(
					this.shape.getSat(this),
					other.shape.getSat(other),
					response,
				);
			} else if (isCircle(other.shape)) {
				collides = sat.testCircleCircle(
					this.shape.getSat(this),
					other.shape.getSat(other),
					response,
				);
			}

		} else if (isPolygon(this.shape)) {
			if (isPolygon(other.shape)) {
				collides = sat.testPolygonPolygon(
					this.shape.getSat(this),
					other.shape.getSat(other),
					response,
				);
			} else if (isCircle(other.shape)) {
				collides = sat.testPolygonCircle(
					this.shape.getSat(this),
					other.shape.getSat(other),
					response,
				);
			}
		}

		return collides;
	}

	public serialize() {
		const clone: any = {};
		Object.assign(clone, this);
		delete clone.game;
		return clone;
	}

	private applyDrag(m: number) {
		if (this.chars.ROTATION_DRAG) {
			this.rSpeed *=  1 - (this.chars.ROTATION_DRAG) * m;
		}

		if (this.chars.DRAG) {
			const speed = this.speed;
			speed.scale(1 - this.chars.DRAG * m);
			this.xSpeed = speed.x;
			this.ySpeed = speed.y;
		}
	}

}
