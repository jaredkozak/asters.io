import { Circle } from './shapes/circle';
import { Polygon } from './shapes/polygon';

export interface Shape {

	type: 'circle' | 'triangle' | 'polygon';
	color: string;

}

export function isCircle(a): a is Circle {
	return a.type === 'circle';
}

export function isPolygon(a): a is Polygon {
	return a.type === 'polygon' || a.type === 'triangle';
}
