import { Vector } from 'sat';
import { EVENTS } from '../common/events';
import { INPUT } from '../common/input';
import { ClientInput } from '../common/pub/v1/ClientInput';
import { InitClient } from '../common/pub/v1/InitClient';
import { Repopulate } from '../common/sub/v1/Repopulate';
import { GameObject } from '../game/GameObject';
import { Ship } from '../game/objects/ship';
import { Circle } from '../game/shapes/circle';
import { Triangle } from '../game/shapes/triangle';
import { isShip } from '../game/types';
import { CLIENT } from './clientConstants';
import { Data } from './data';
import { currentSpace, drawMap, MAX_MAP_ZOOM, MIN_MAP_ZOOM } from './map';
import { Star } from './Star';

let gameWorker: Worker;

const MAX_STAR_DISTANCE = 8;
const MIN_STAR_DISTANCE = 2;

const MAX_STAR_SIZE = 5;
const MIN_STAR_SIZE = 1;

const MIN_ZOOM = 0.01;
const MAX_ZOOM = 3.5;

function createWorker() {
	gameWorker = new Worker('/js/logic.js');

	gameWorker.onmessage = (ev) => {
		if (ev.data.type === 'objects') {
			gameObjects = ev.data.msg;
		}
	};

	start();
}

function sendEvent(type: EVENTS, msg: any, to?: string) {
	gameWorker.postMessage(
		{
			type,
			msg,
			to,
		},
	);
}

function sendEventToPlayer(type: EVENTS, msg: any) {
	if (player) {
		sendEvent(type, msg, player.id);
	}
}

const data = new Data();

let gameObjects: GameObject[] = [];
let player: Ship;
let zoom = 1;
let mapZoom = 1;
let c: HTMLCanvasElement;
let $: CanvasRenderingContext2D;
const animating = true;
let lastTime = 0;
let FPS = 0;
let connection: InitClient.ClientConnection;

const camera = new Vector(0, 0);
const lastCamera = new Vector(0, 0);
let cameraVel = new Vector(0, 0);

const KEYS = {
	UP: 'KeyW',
	DOWN: 'KeyS',
	LEFT: 'KeyQ',
	RIGHT: 'KeyE',
	TURN_LEFT: 'KeyA',
	TURN_RIGHT: 'KeyD',
	TOGGLE_INERTIAL_DAMPENERS: 'KeyX',
	TOGGLE_MAP: 'KeyM',
	SHOOT_TORPEDO: 'Space',
	DIE: 'Backspace',
};

const INIT_MAP_MODE = 0.02;
let mapModeEnabled = false;
let lastMouseClicking = false;
let mouseClicking = false;
const mouse = {
	x: 0,
	y: 0,
};

const currentKeys: { [code: string]: boolean } = {};
let lastKeys = currentKeys;
let highScore: Repopulate.HighScore;

export function getMousePos() {
	return new Vector(mouse.x, mouse.y);
}

export function isMouseUnpressed() {
	return !mouseClicking && lastMouseClicking;
}

export function isMousePressed() {
	return mouseClicking && !lastMouseClicking;
}

export function isMouseDown() {

	return mouseClicking;
}


/**
 * Whether the key is currently pressed
 * @param code The key code (eg. KeyQ, LeftControl, etc)
 */
function isKeyDown(code: string) {
	return currentKeys[code] || false;
}

/**
 * Whether the key has been clicked down (happens once)
 * @param code The key code (eg. KeyQ, LeftControl, etc)
 */
function isKeyPressed(code: string) {
	return currentKeys[code] && !lastKeys[code];
}

/**
 * Whether the key has been clicked down (happens once)
 * @param code The key code (eg. KeyQ, LeftControl, etc)
 */
function isKeyReleased(code: string) {
	return !currentKeys[code] && lastKeys[code];
}

function checkInputs() {

	if (!player) {
		return;
	}

	function checkHeldInput(key, input) {

		if (isKeyPressed(key)) {
			sendEventToPlayer(EVENTS.INPUT_START, input);
			data.publish<ClientInput.Input, ClientInput.Output>(
				ClientInput.action, {
					inputType: input,
					start: true,
				});
		}
		if (isKeyReleased(key)) {
			sendEventToPlayer(EVENTS.INPUT_END, input);
			data.publish<ClientInput.Input, ClientInput.Output>(
				ClientInput.action, {
					inputType: input,
					start: false,
				});
		}
	}

	checkHeldInput(KEYS.UP, INPUT.FIRE_UP);
	checkHeldInput(KEYS.DOWN, INPUT.FIRE_DOWN);
	checkHeldInput(KEYS.LEFT, INPUT.FIRE_LEFT);
	checkHeldInput(KEYS.RIGHT, INPUT.FIRE_RIGHT);
	checkHeldInput(KEYS.TURN_LEFT, INPUT.FIRE_ROTATE_LEFT);
	checkHeldInput(KEYS.TURN_RIGHT, INPUT.FIRE_ROTATE_RIGHT);

	if (isKeyPressed(KEYS.TOGGLE_INERTIAL_DAMPENERS)) {
		if (!player.attr[INPUT.INERTIAL_DAMPENERS_ON]) {
			sendEventToPlayer(EVENTS.INPUT_START, INPUT.INERTIAL_DAMPENERS_ON);
			data.publish<ClientInput.Input, ClientInput.Output>(
				ClientInput.action, {
					inputType: INPUT.INERTIAL_DAMPENERS_ON,
					start: true,
				});
		} else {
			sendEventToPlayer(EVENTS.INPUT_END, INPUT.INERTIAL_DAMPENERS_ON);
			data.publish<ClientInput.Input, ClientInput.Output>(
				ClientInput.action, {
					inputType: INPUT.INERTIAL_DAMPENERS_ON,
					start: false,
				});
		}
	}
	if (isKeyPressed(KEYS.TOGGLE_MAP)) {
		mapModeEnabled = !mapModeEnabled;
		if (mapModeEnabled) {
			zoom = MIN_ZOOM;
		} else {
			mapZoom = MAX_MAP_ZOOM;
		}
	}

	if (isKeyPressed(KEYS.SHOOT_TORPEDO)) {
		sendEventToPlayer(EVENTS.INPUT_START, INPUT.SHOOT_TORPEDO);
		data.publish<ClientInput.Input, ClientInput.Output>(
			ClientInput.action, {
				inputType: INPUT.SHOOT_TORPEDO,
				start: true,
			});
	}
	if (isKeyPressed(KEYS.DIE)) {
		localStorage.clear();
		location.reload();
	}

}

function pointToCamera(p: Vector) {

	const center = new Vector(c.width / (2), c.height / (2));

	const newPoint = p.clone().sub(camera.clone()).scale(zoom).add(center);

	return newPoint;
}

function moveCamera() {
	if (!player) {
		return;
	}
	const playerPosInCam = pointToCamera(new Vector(player.x, player.y));

	const center = new Vector(c.width / (2), c.height / (2));
	const distance = playerPosInCam.clone().sub(center);
	if (distance.len() > CLIENT.cameraBounds * c.width) {
		const adjustedDistance = distance.clone()
			.normalize()
			.scale(CLIENT.cameraBounds * c.width)
			.sub(distance);
		camera.sub(adjustedDistance);
	}

	cameraVel = lastCamera.clone().sub(camera);
	lastCamera.x = camera.x;
	lastCamera.y = camera.y;
}

const stars: Star[] = [];

function drawStars() {

	const STAR_X = c.width * 2;
	const STAR_Y = c.height * 2;

	for (const star of stars) {
		$.beginPath();
		$.arc(
			star.pos.x, // x (middle)
			star.pos.y, // y (middle)
			star.size / star.distance, // radius
			0, 2 * Math.PI, // arc start, arc end
			false, // anticlockwise
		);

		$.fillStyle = 'white';
		$.fill();
		$.closePath();

		star.pos.x += cameraVel.x / star.distance;
		star.pos.y += cameraVel.y / star.distance;

		if (star.pos.x < 0) { star.pos.x += STAR_X; }
		if (star.pos.x >= STAR_X) { star.pos.x -= STAR_X; }
		if (star.pos.y < 0) { star.pos.y += STAR_Y; }
		if (star.pos.y >= STAR_Y) { star.pos.y -= STAR_Y; }
	}
}

function setHighScore(score: Repopulate.HighScore) {
	if (!player) { return; }
	highScore = score;
}

function drawHighScore() {
	if (!highScore || !player) { return; }

	const relative = pointToCamera(new Vector(highScore.pos.x, highScore.pos.y));
	const offScreen = relative.x < 0 || relative.y > c.width || relative.y < 0 || relative.y > c.height;

	if (offScreen) {
		$.beginPath();

		const OFFSET = 50;
		const SIZE = 20;
		const WIDTH = 4;
		$.arc(
			relative.x, // x (middle)
			relative.y - OFFSET, // y (middle)
			SIZE, // radius
			0, 1 * Math.PI, // arc start, arc end
			false, // anticlockwise
		);

		$.strokeStyle = 'yellow';
		$.lineWidth = WIDTH * zoom;
		$.stroke();
		$.closePath();
	} else {
		$.beginPath();

		const OFFSET = 50;
		const SIZE = 20;
		const WIDTH = 4;
		$.arc(
			relative.x, // x (middle)
			relative.y - OFFSET, // y (middle)
			SIZE, // radius
			0, 1 * Math.PI, // arc start, arc end
			false, // anticlockwise
		);

		$.strokeStyle = 'yellow';
		$.lineWidth = WIDTH * zoom;
		$.stroke();
		$.closePath();
	}
}

function drawGame() {

	for (const go of gameObjects) {
		if (isShip(go) && go.chars.player) {
			if (!player) {
				camera.x = go.x;
				camera.y = go.y;
				lastCamera.x = go.x;
				lastCamera.y = go.y;
				console.log(camera.x, go.x);
			}
			player = go;
			moveCamera();
			const TOP = 25;
			$.font = `20pt Calibri`;
			$.textAlign = 'center';
			$.fillStyle = 'white';
			$.textBaseline = 'bottom';
			$.fillText(go.name, c.width / 2, c.height - TOP);

			$.font = `15pt Calibri`;
			$.textAlign = 'center';
			$.fillStyle = 'yellow';
			$.textBaseline = 'top';
			$.fillText(`SCORE: ${ Math.round(player.attr.score) }`, c.width / 2, TOP);

		}
		const r = go.r;

		const objPos = pointToCamera(new Vector(go.x, go.y));

		switch (go.shape.type) {
		case 'circle':
			let circle = new Circle(0);
			// tslint:disable-next-line:prefer-object-spread
			circle = Object.assign(circle, go.shape);
			$.beginPath();

			$.arc(
				objPos.x, // x (middle)
				objPos.y, // y (middle)
				circle.radius, // radius
				0, 2 * Math.PI, // arc start, arc end
				false, // anticlockwise
			);
			$.fillStyle = go.shape.color;
			$.fill();
			$.closePath();
			break;
		case 'triangle':
			let triangle = new Triangle(0, 0);
			// tslint:disable-next-line:prefer-object-spread
			triangle = Object.assign(triangle, go.shape);

			const points = triangle.getPoints(go.x, go.y, r);

			$.beginPath();
			for (let i = 0; i < points.length; i++) {
				const pointPos = pointToCamera(new Vector(points[i].x, points[i].y));

				if (i === 0) {
					$.moveTo(pointPos.x, pointPos.y);
				} else {
					$.lineTo(pointPos.x, pointPos.y);
				}
			}

			$.fillStyle = go.shape.color;
			$.fill();
			$.closePath();
			break;
		}

		if (go !== player && isShip(go)) {
			const BASE_SIZE = 15;
			const size = BASE_SIZE * zoom;
			$.font = `${ size }pt Calibri`;
			$.textAlign = 'center';
			$.fillStyle = 'white';
			$.textBaseline = 'top';
			$.fillText(go.name, objPos.x, objPos.y);
		}

	}
}

function getTitle() {
	return `Asters.IO - ${ currentSpace.name }`;
}

function changeSystem() {
	history.replaceState({}, getTitle(), `/${ currentSpace.id }`);
}

async function start() {

	c = document.createElement('canvas');

	// tslint:disable-next-line:only-arrow-functions
	c.addEventListener('mousemove', function(evt) {
		const rect = c.getBoundingClientRect();
		mouse.x = evt.clientX - rect.left;
		mouse.y = evt.clientY - rect.top;
	}, false);

	// tslint:disable-next-line:only-arrow-functions
	window.addEventListener('wheel', function(evt) {
		const scrollScale = 15;
		if (mapModeEnabled) {
			zoomMap();
		} else {
			zoomGame();
		}

		function zoomMap() {
			mapZoom += scrollScale / -evt.deltaY;

			if (mapZoom < MIN_MAP_ZOOM) {
				mapZoom = MIN_MAP_ZOOM;
			}

			if (mapZoom > MAX_MAP_ZOOM) {
				mapZoom = MAX_MAP_ZOOM;
				zoom = MIN_ZOOM;
				mapModeEnabled = false;
			}
		}

		function zoomGame() {

			zoom += scrollScale / -evt.deltaY;

			if (zoom < MIN_ZOOM) {
				zoom = MIN_ZOOM;
				mapModeEnabled = true;
				mapZoom = MAX_MAP_ZOOM;
			}

			if (zoom > MAX_ZOOM) {
				zoom = MAX_ZOOM;
			}
		}
	}, { capture: true, once: false, passive: true });

	// tslint:disable-next-line:only-arrow-functions
	window.addEventListener('keydown', function(evt) {
		currentKeys[evt.code] = true;
	});

	// tslint:disable-next-line:only-arrow-functions
	window.addEventListener('keyup', function(evt) {
		currentKeys[evt.code] = false;
	});

	window.addEventListener('mousedown', () => {
		mouseClicking = true;
	}, false);
	window.addEventListener('mouseup', () => {
		mouseClicking = false;
	}, false);
	window.addEventListener('mouseleave', () => {
		mouseClicking = false;
	}, false);
	window.addEventListener('mouseout', () => {
		mouseClicking = false;
	}, false);

	$ = c.getContext('2d');

	c.width = window.innerWidth;
	c.height = window.innerHeight;

	window.onresize = (e) => {
		c.width = window.innerWidth;
		c.height = window.innerHeight;
	};

	document.body.appendChild(c);

	const STAR_COUNT = 100;
	const STAR_X = c.width * 2;
	const STAR_Y = c.height * 2;
	for (let i = 0; i < STAR_COUNT; i++) {
		const x = Math.random() * STAR_X - c.width / 2;
		const y = Math.random() * STAR_Y - c.height / 2;
		const size = MIN_STAR_SIZE + Math.random() * (MAX_STAR_SIZE - MIN_STAR_SIZE);
		const distance = MIN_STAR_DISTANCE + Math.random() * (MAX_STAR_DISTANCE - MIN_STAR_DISTANCE);
		stars.push(new Star(new Vector(x, y), size, distance));
	}

	data.clientId = localStorage.getItem(CLIENT.clientIdStorage);
	data.clientSecret = localStorage.getItem(CLIENT.clientSecretStorage);

	connection = await data.publish<InitClient.Input, InitClient.Output>(
		InitClient.action,
	);

	if (!connection && connection.id && connection.secret) {
		console.error('Server did not return active connection.');
		return;
	}

	localStorage.setItem(CLIENT.clientIdStorage, connection.id);
	localStorage.setItem(CLIENT.clientSecretStorage, connection.secret);
	data.clientId = connection.id;
	data.clientSecret = connection.secret;
	console.log(`Logged in as ${ connection.name }`);

	data.startSubscription();
	data.onReceive().subscribe((res) => {
		// console.log(res);
		// TODO: sendEvent(res)
	});

	data.onReceive(Repopulate.action)
		.subscribe((res) => {
			setHighScore(res.data.highScore);
			sendEvent(EVENTS.REPOPULATE, res.data);
		});

	// Change to current players system
	changeSystem();

	// Setup draw loop
	const draw = (time) => {

		if (!animating) {
			return;
		}

		// tslint:disable-next-line:no-magic-numbers
		FPS = (1 / (time - lastTime)) * 1000;

		$.clearRect(0, 0, c.width, c.height);
		drawStars();

		if (mapModeEnabled) {
			drawMap($, c, {
				mapZoom,
			});
		} else {
			drawGame();
		}

		// drawHighScore();

		if (player && (player.attr.destroyed || player.health <= 0)) {
			localStorage.clear();
			location.reload(true);
			return;
		}

		checkInputs();

		lastKeys = { ...currentKeys };
		lastTime = time;
		window.requestAnimationFrame(draw);
		lastMouseClicking = mouseClicking;
	};

	window.requestAnimationFrame(draw);

	console.log('Game started');
}

if (typeof(Worker) !== 'undefined') {

	window.onload = createWorker;
} else {
	// Sorry! No Web Worker support..
	alert('Your browser does not support this application, sorry!');
}
