
export interface ObjectCharacteristics {

	// Ship will lose this percentage of rotation per second
	ROTATION_DRAG: number;
	// Ship will lose this percentage of speed per second
	DRAG: number;

	VIEW_DISTANCE: number;
	// Whether this is the current player
	player?: boolean;
}

export interface ShipCharacteristics extends ObjectCharacteristics {
	FWD_THRUST: number;
	REVERSE_THRUST: number;
	SIDE_THRUST: number;
	ROTATE_THRUST: number;
	TORPEDO_SPEED: number;

	WIDTH: number;
	HEIGHT: number;

}
