import * as codes from 'http-status-codes';
import { OK } from 'http-status-codes';
import { Subscription } from 'rxjs';
import { ClientCredentials } from '../../common/ClientCredentials';
import { TEN_SECONDS } from '../../common/constants';
import { Log } from '../../common/log';
import { SubMessage } from '../../common/sub/SubMessage';
import { Heartbeat } from '../../common/sub/v1/Heartbeat';
import { ServerParams } from '../interfaces/ServerParams';

const subLog = new Log('Sub');

export async function initSubRoutes(params: ServerParams) {

	params.app.post('/server/sub', (req, res) => {

		if (!req.body || !req.body.id || !req.body.secret) {
			res.status(codes.UNAUTHORIZED);
			res.end();
			return;
		}

		const creds = req.body as ClientCredentials;

		const client = params.network.findAndValidate(creds.id, creds.secret);
		if (!client) {
			res.status(codes.BAD_REQUEST);
			res.end();
			return;
		}

		if (client.connected) {
			res.status(codes.CONFLICT);
			res.end();
			return;
		}

		client.connected = true;
		let open = true;
		let subscription: Subscription;
		const log = new Log(`Con - ${ client.name }`);

		const end = () => {
			open = false;
			client.connected = false;
			res.end();
			this.log.info(`${ client.name } left`);
			subscription.unsubscribe();
		};

		req.on('close', end);
		req.on('end', end);

		res.writeHead(OK, {
			'Content-Type': 'application/json',
			'Transfer-Encoding': 'chunked',
		});

		function send(msg: SubMessage) {
			if (client.connected && open) {
				log.trace(msg);
				res.write(JSON.stringify(msg) + '\n');
			}
		}

		function heartbeat() {
			setTimeout(() => {

				if (client.connected && open) {
					send({ action: Heartbeat.action });
					heartbeat();
				}
			}, TEN_SECONDS);
		}

		subscription = client.outMessages
			.subscribe((msg: SubMessage) => {
				send(msg);
			});

		send({ action: Heartbeat.action });
		heartbeat();
		log.debug(`Dispatching unsent messages: ${ client.dispatchUnsentMessages() }`);
	});
}
