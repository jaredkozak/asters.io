import * as bodyParser from 'body-parser';
import * as express from 'express';
import { Server } from 'http';
import { join } from 'path';
import { Log } from '../common/log';
import { Game } from '../game/Game';
import { config } from './config';
import { ServerParams } from './interfaces/ServerParams';
import { ServerNetwork } from './network';
import { initPubRoutes } from './routes/pub';
import { initSubRoutes } from './routes/sub';

const log = new Log('GameServer');
export let app: express.Express;
export let game: Game;
export let network: ServerNetwork;
export let server: Server;

export async function initGameServer() {

	app = express();
	game = new Game();
	network = new ServerNetwork(game);
	const params: ServerParams = {
		app,
		game,
		network,
	};
	await configureServer(params);
}

export async function startGameServer() {

	server = this.app.listen(config.PORT);
	log.info(`Server started on port ${ config.PORT }`);
	game.start();
}

export async function configureServer(params: ServerParams) {

	app.use(bodyParser.json());
	app.use('/', express.static(join(__dirname, '../../public')));
	app.get('/**', (req: express.Request, res: express.Response, next) => {
		return res.sendFile(join(__dirname, '../../public/index.html'));
	});

	initPubRoutes(params);
	initSubRoutes(params);

}
