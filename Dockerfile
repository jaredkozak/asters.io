FROM node:latest

# Run the startup script
WORKDIR /usr/app

COPY . /usr/app
RUN yarn cache clean
RUN yarn install

EXPOSE 5000


CMD [ "./startup.sh" ]