import * as Sentencer from 'sentencer';
import { EVENTS } from '../../../common/events';
import { gameOptions } from '../../../common/gameOptions';
import { Log } from '../../../common/log';
import { InitClient } from '../../../common/pub/v1/InitClient';
import { Game } from '../../../game/Game';
import { Ship } from '../../../game/objects/ship';
import { ClientConnection } from '../../ClientConnection';
import { PubParams } from '../../interfaces/PubParams';

const log = new Log('Init Client');

export async function handleInitClient(params: PubParams): Promise<InitClient.Output> {
	let client = params.client;
	const game = params.server.game;

	let con: InitClient.ClientConnection;

	if (client) {
		const secret = client.genSecret();
		con = {
			id: client.id,
			secret,
			name: client.name,
		};
		this.log.info(`${ client.name } connected`);
	} else {

		client = new ClientConnection(this.game, this.network);
		const secret = client.genSecret();
		const name: string = Sentencer.make('{{ adjective }} {{ noun }}');
		client.setName( name );
		this.log.info(`${ name } joined`);
		client.heartbeat();
		this.createShip(game, client);
		con = {
			id: client.id,
			secret,
			name: client.name,
		};
	}

	return {
		data: con,
	};
}

export async function createShip(game: Game, client: ClientConnection) {

	const ship = new Ship(client.id);

	ship.name = client.name;
	ship.x = Math.random() * gameOptions.spawnArea - gameOptions.spawnArea / 2;
	ship.y = Math.random() * gameOptions.spawnArea - gameOptions.spawnArea / 2;
	log.info(`${ client.name } spawned at ${ Math.round(ship.x) }, ${ Math.round(ship.y) }`);

	game.sendEvent(EVENTS.SPAWN_SHIP, {
		ship,
	});
}
