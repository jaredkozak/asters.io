import { Response } from 'sat';
import { GAME_UPDATE_PERIOD } from '../common/constants';
import { EVENTS } from '../common/events';
import { Log } from '../common/log';
import { Repopulate } from '../common/sub/v1/Repopulate';
import { dataToGameObject } from './dataToGameObject';
import { GAME } from './GameConsants';
import { GameObject } from './GameObject';
import { Loop } from './loop';
import { Ship } from './objects/ship';
import { Torpedo } from './objects/torpedo';
import { isShip } from './types';

export class Game {

	public loop = new Loop();
	public gameObjects: GameObject[] = [];
	public currentPlayer: Ship;

	private log = new Log('Game');

	public constructor() {
		this.loop.addHandler({
			period: GAME_UPDATE_PERIOD,
			update: (delta) => { this.update(delta); },
		});
	}

	public async start() {
		// const ship = new Ship();
		// // ship.xAccel = 5;
		// // ship.rAccel = 0.1;
		// ship.chars.player = true;

		// this.gameObjects.push(ship);

		this.eventListener();
		this.loop.start();
	}

	public async update(delta: number) {

		for (const go of this.gameObjects) {
			await go.update(delta);
		}

		this.doCollisionDetection(delta);

		if (GAME.isWorker) {
			self.postMessage(
				{
					type: 'objects',
					msg: this.gameObjects.map((g) => g.serialize()),
				},
				undefined,
			);
		}
	}

	public doCollisionDetection(delta) {
		const checked: { [ objAId: string ]: { [ objBId: string ]: true } } = {  };
		const response = new Response();
		for (const a of this.gameObjects) {
			checked[a.id] = {};
			for (const b of this.gameObjects) {

				// Skip if the same
				if (a === b) { continue; }

				// Skip if already checked
				if (checked[b.id] && checked[b.id][a.id]) { continue; }

				const collides = a.checkCollision(b, response);
				if (collides) {
					const HALF = 0.5;
					const overlap = response.overlapV.scale(HALF);
					a.pos = a.pos.sub(overlap);
					b.pos = b.pos.add(overlap);

					const aForce = response.overlapV.clone()
						.scale(b.bounce * b.mass)
						.project(a.speed.add(b.speed));

					const bForce = response.overlapV.clone()
						.scale(a.bounce * a.mass)
						.reverse()
						.project(a.speed.add(b.speed));

					// TODO: apply force at collision point
					a.applyForceAtCenter(aForce, delta);
					b.applyForceAtCenter(bForce, delta);
					a.onCollision(b, response);
					b.onCollision(a, response);

					// this.log.logInfoLimit('collides ' + a.id, 2, aForce, bForce);
				}

				// check a vs b
				// have effect on a and b
				checked[a.id][b.id] = true;
				response.clear();
			}
		}
	}

	public sendEvent(type: number, msg: any, to?: string) {

		let go: GameObject;
		if (to) {
			go = this.findById(to);
		}

		switch (type) {
			case EVENTS.INPUT_START:
				if (go instanceof Ship) { go.inputStart(msg); }
				break;
			case EVENTS.INPUT_END:
				if (go instanceof Ship) { go.inputEnd(msg); }
				break;
			case EVENTS.REPOPULATE:
				const ev = msg as Repopulate.Event;
				const newObjects = [];
				// this.log.logInfoLimit('repop', 2, ev.nearby);
				for (const nGo of ev.nearby) {
					const obj = dataToGameObject(nGo);
					obj.setGame(this);

					if (isShip(obj) && obj.id === ev.currentPlayer) {
						obj.chars.player = true;
						this.currentPlayer = obj;
					}

					newObjects.push(obj);

				}
				this.gameObjects = newObjects;
				// this.log.logInfoLimit('repop', 2, newObjects);

				break;
			case EVENTS.SPAWN_SHIP:
				if (msg.ship instanceof Ship) {
					const ship = new Ship(msg.id);
					Object.assign(ship, msg.ship);
					ship.setGame(this);
					this.gameObjects.push(ship);
					return ship;
				}
				break;
			default:
				console.warn(`Unhandled event: ${ type }`, msg);
		}
	}

	/**
	 * Find a game object by id
	 * @param id The id of the game object to look for
	 */
	public findById(id: string) {
		for (const go of this.gameObjects) {
			if (go.id === id) {
				return go;
			}
		}
		return undefined;
	}

	public removeById(id: string) {
		const index = this.gameObjects.findIndex((go) => go.id === id);
		return this.gameObjects.splice(index, 1)[0];
	}

	public eventListener() {

		if (GAME.isWorker) {
			self.onmessage = (msg) => {
				this.sendEvent(msg.data.type, msg.data.msg, msg.data.to);
			};
		}

	}
}
