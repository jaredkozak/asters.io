import { NOT_IMPLEMENTED } from 'http-status-codes';
import { Route } from '../interfaces/Route';

export class MyRoute extends Route {

	public async load() {
		this.app.get('/NOT_IMPLEMENTED', (req, res) => {
			res.sendStatus(NOT_IMPLEMENTED);
			res.send('Not Implemented Yet.');
			this.log.error('Route not yet implemented!');
			res.end();
		});
	}
}
