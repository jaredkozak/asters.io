
export interface ClientCredentials {

	// Client UUID
	id: string;

	// Client secret
	secret: string;
}
