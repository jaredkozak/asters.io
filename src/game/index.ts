import { Game } from './Game';
import { GAME } from './GameConsants';

GAME.isBrowser = true;
GAME.isServer = false;

if (!self.document) {
	// I am a web worker!
	GAME.isWorker = true;
	const game = new Game();

	game.start();

}
