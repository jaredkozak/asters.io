
export namespace Heartbeat {

	export const action = '$HEARTBEAT';
	export interface Input {}
	export interface Output {}

}
