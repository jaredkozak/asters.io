
// tslint:disable-next-line:no-namespace
export namespace ClientState {
	export const action = 'clientState';

	export interface Input {

		x: number;
		y: number;
		sX: number;
		xY: number;
		aX: number;
		aY: number;
	}

	export interface Output {}
}
